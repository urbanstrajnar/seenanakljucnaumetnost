package nakljucnaUmetnost.main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import nakljucnaUmetnost.okno.GumbNarisi;
import nakljucnaUmetnost.okno.Okno;
import nakljucnaUmetnost.okno.Platno;

public class Main {

	public static void main(String[] args) {
		
		Okno okno = new Okno("Naklju�na Umetnost");
		
		//dodaj komponente
		
		
		//dodaj panel
		JPanel pane = new JPanel();
		pane.setLayout(new GridBagLayout());
		okno.add(pane);
		
		//layout manager
		GridBagConstraints c = new GridBagConstraints();
		c.gridheight = 2;
		c.gridwidth = 1;
		
		
		//dodaj platno
		Platno platno = new Platno();
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridx = 0;
		c.gridy = 0;
		c.ipady = 400;
		c.ipadx = 200;
		c.weightx = 1.0;
		c.weightx = 1.0;
		
		pane.add(platno, c);
		
		//dodaj gumb
		GumbNarisi gumbNarisi = new GumbNarisi("Nari�i", platno);
		c.anchor = GridBagConstraints.PAGE_END;
		c.gridx = 0;
		c.gridy = 1;
		c.ipady = 0;
		c.ipadx = 0;
		c.weightx = 0.0;
		c.weightx = 0.0;
		pane.add(gumbNarisi, c);
		
		okno.pack();
		okno.setVisible(true);

	}

}
