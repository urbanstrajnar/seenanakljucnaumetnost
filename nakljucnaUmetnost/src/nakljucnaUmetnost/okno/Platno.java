package nakljucnaUmetnost.okno;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Platno extends JPanel{

	public Platno() {
		
	}
	
	@Override
	public void paint(Graphics g){
		g.setColor(Color.white);
		g.fillRect(0,0,200,200);
		int r = (int) (Math.random()*255);
		int gr = (int) (Math.random()*255);
		int b = (int) (Math.random()*255);
		g.setColor(new Color(r,gr,b));
		g.fillRect(0, 0, (int) (Math.random() * 200), (int) (Math.random() * 200));
	}
	
}
