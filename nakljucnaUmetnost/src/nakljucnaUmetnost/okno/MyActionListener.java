package nakljucnaUmetnost.okno;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener{

	
	Platno platno;
	
	public MyActionListener(Platno platno){
		super();
		this.platno = platno;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		platno.repaint();
		
	}

}
